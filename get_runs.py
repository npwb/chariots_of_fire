import random
import requests
from bs4 import BeautifulSoup
from agents import agents


def get_list_of_runs():
    r = requests.get('https://www.parkrun.org.uk/wp-content/themes/parkrun/xml/geo.xml',
                     headers={'user-agent': random.choice(agents)})
    return sorted([place['n'] for place in BeautifulSoup(r.text, "html.parser").find_all('e')])
