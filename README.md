# Chariots of Fire

A simple set of scripts to devise teams of six for the Cambridge-based charity relay race, Chariots of Fire.

## Requirements

* Python 3 + standard libraries
* Poetry

## Background

Twelve of us wanted a way of making teams who would be as similar as possible (to compete against each other - we aren't directly competing against anyone else!).
Since we've all done the same parkrun together, we decided to use our times in this parkrun to sort out the teams (on the assumption that a runner's 5k time is
a reasonable indication of their 3k running ability).

## How to use it

1. Update the athletes.py script so that the athletes dictionary contains the details of twelve runners (the keys should be the runners' names, the values can
   either be their parkrun IDs as integers, or a string representation of their 5k time - e.g. "25:30").
2. Run the CoF.py script ($ poetry run python3 CoF.py). It will ask how you would like the parkrun times to be selected, for those athletes who haven't just
   been given a string representation of their time. If relevant it will also ask which course you want to use (and perform elementary validation against the
   list here: https://www.parkrun.org.uk/wp-content/themes/parkrun/xml/geo.xml).
3. In a few seconds, the teams will be generated! Good luck!

## "Scientific" methodology (included by popular request)

agents.py:    this is a list of user-agents. Used for convincing the parkrun website that we're a human and not a script.

athletes.py:  this is a dictionary containing a runner's name and their parkrun ID. Should have twelve entries.

get_runs.py:  this script scrapes the parkrun website for a list of all events and returns their names in lowercase and alphabetical order

get_times.py: this script scrapes the parkrun website for the runner's best, mean or worst times at the parkrun in question.
              Returns a dictionary to feed into the final script.

CoF.py:       the script that does the processing. How it works, in English:

1. Adds up the total number of seconds in the twelve times and divides this by two. This is the target to aim for.
2. Creates a generator of all 462 useful groups of six (i.e. the first half of the 924 total combinations) - this means the first entry in the dictionary
   will always be in team 1, assuming you're using Python 3.6+.
3. For each possible team, sums their times, then compares the absolute difference between the target time and this total with the difference between the
   currently-closest team's time and the target time
4. If a team has a smaller difference, this means this team arrangement is better than the previous one. Forget the last one and save this new one.
5. At the end, work out who's in the other team (i.e. everyone who isn't in the first team) and print the teams.
