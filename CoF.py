from itertools import combinations
from get_times import get_times
from get_runs import get_list_of_runs


def divide_teams(variety, run, average=None):
    times = get_times(variety, run, average)
    target = distance = sum([k for _, k in times.items()]) / 2  # initialising the target to half the total seconds
    t1 = fast = None  # no-one is initially faster
    groups = list(combinations(times.items(), 6))[:462]  # this is 2-3 times quicker than just exhausting the generator
    for g in groups:
        total = sum([x[1] for x in g])
        if abs(target - total) < distance:
            distance = abs(target - total)
            t1 = [x[0] for x in g]
            fast = "Team 1" if total < target else "Team 2" if total > target else "Equal"
    
    t2 = [n for n in times if n not in t1]
    print("Team 1 is {}.\nTeam 2 is {}.\nTime difference: {} seconds.\nAdvantage: {}.".format(t1, t2,
                                                                                              round(2*distance, 2),
                                                                                              fast))


if __name__ == "__main__":
    typ = int(input("""The type parameter reflects which times you wish to compare:
1: PBs
2: average times
3: worst times
4: best times this year
5: average of most recent runs
6: average of most recent runs (all parkruns)
7: average of all runs this year (all parkruns)
Choice: """))
    while typ not in {1, 2, 3, 4, 5, 6, 7}:
        typ = int(input("Please enter one of: 1, 2, 3, 4, 5, 6, 7."))
    av = int(input("How many runs do you want to average? ")) if typ in {5, 6} else None
    course = None
    if typ not in {6, 7}:
        course = input("Please type the name of the parkrun you wish to use:\n").lower().replace(" ", "")
        runs = get_list_of_runs()
        while course not in runs:
            temp_runs = [r for r in runs]
            temp_runs.append(course)
            temp_runs = sorted(temp_runs)
            ind = temp_runs.index(course)
            closest = [ind - 1, ind + 1] if 0 < ind <= len(temp_runs) - 2 else [ind - 1] if 0 < ind else [ind + 1]
            course = input("""Not a recognised parkrun (converted form: {})
Closest match(es) alphabetically: {}
Please type the name of the parkrun you wish to use:
""".format(course, ", ".join([temp_runs[c] for c in closest]))).lower().replace(" ", "")
    divide_teams(typ, course, av)
