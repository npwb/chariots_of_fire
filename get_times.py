def get_times(typ, parkrun, average=None):
    import datetime
    import random
    import re
    import requests
    import time
    from bs4 import BeautifulSoup
    from athletes import athletes
    from agents import agents

    location_url = 'https://www.parkrun.org.uk/{}/results/athletehistory/?athleteNumber={}'
    athlete_url = 'https://www.parkrun.org.uk/results/athleteresultshistory/?athleteNumber={}'
    times = dict()
    # times are in "MM:SS" strings - need to convert them to seconds
    s = lambda t: 60 * int(t.split(":")[0]) + int(t.split(":")[1])
    # look at the page for the athlete and take the appropriate time
    for name, number in athletes.items():
        # 2021 enhancement: some people aren't parkrunners! So they give us an estimated time and we incorporate it
        if not isinstance(number, int):
            times[name] = s(number)
            continue
        if typ not in {6, 7}:
            r = requests.get(location_url.format(parkrun, number), headers={'user-agent': random.choice(agents)})
            results = BeautifulSoup(r.text, features="html.parser")
            if typ in {1, 2, 3}:
                table = [t.find_parent('table') for t in results.find_all('caption') if t.get_text() == 'Summary Stats'][0]
                times[name] = s(table.find_all('tr')[1].find_all('td')[typ].text)
            elif typ == 4:
                table = [t.find_parent('table') for t in results.find_all('caption')
                         if t.get_text() == 'Best Annual Achievements'][0]
                times[name] = s(table.find_all('tr')[-1].find_all('td')[1].text)
            elif typ == 5:
                table = [t.find_parent('table') for t in results.find_all('caption')
                         if t.get_text() == 'All Results'][0]
                try:
                    fields = table.find('tbody').find_all('tr')[:average]
                except IndexError:
                    fields = table.find('tbody').find_all('tr')
                times[name] = sum([s(t.find_all('td')[3].text) for t in fields]) / average
        else:
            r = requests.get(athlete_url.format(number), headers={'user-agent': random.choice(agents)})
            results = BeautifulSoup(r.text, features="html.parser")
            table = results.find_all(id="results")[0]
            if typ == 6:
                try:
                    fields = table.find('tbody').find_all('tr')[:average]
                except IndexError:
                    fields = table.find('tbody').find_all('tr')
                times[name] = sum([s(t.find_all('td')[4].text) for t in fields]) / average
            else:
                fields = table.find('tbody').find_all('tr')
                runs_this_year = [run for run in fields if run.find_all('td')[1].text.endswith(str(datetime.datetime.now().year))]
                if len(runs_this_year):
                    times[name] = sum([s(t.find_all('td')[4].text) for t in runs_this_year]) / len(runs_this_year)
                else:
                    raise IndexError(f"{name} hasn't run any parkruns this year!")
        time.sleep(random.random())  # in case parkrun are suspicious
    return times
